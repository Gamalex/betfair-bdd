Feature: Bet placement and returns on English Premier League
  As a Betfair Customer 
  I want the ability to place a bet on a English Premier League event
  So that I can win

@DEVICE_desktop
Scenario Outline: Potential payout is correctly calculated on any Premier League market for non-logged in users
    Given I have access to Betfair's website
    And I have navigated to any Betfair English Premier League football event
    And I have selected the <selection> selection as my betable selection
    When I have selected €<stake> as my bet stake
    Then I can see the returns offered proportional to the selection's odds

    Examples: 
     | selection | stake |
     | home      |  0.05 |

@DEVICE_desktop
Scenario Outline: Placing a bet and calculating returns on any Premier League market for logged in users
    Given I am logged into my Betfair account
    And I have navigated to any Betfair English Premier League football event
    And I have selected the <selection> selection as my betable selection
    And I have selected €<stake> as my bet stake
    When I submit my betslip
    Then I can see my bet successfully placed
    And I can see the potential payout for my bet

    Examples: 
     | selection | stake |
     | home      |  0.20 |
     