package com.betfair.bdd.steps;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.betfair.TestRunner;
import com.betfair.bdd.data.World;
import com.betfair.bdd.pageobjects.Betslip;
import com.betfair.bdd.pageobjects.PageHeader;
import com.betfair.bdd.pageobjects.PageMainZone;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BettingSteps extends TestRunner {

	private World world;
	private WebDriver driver;
	
	/*
	 * Common PO actions used
	 */
	PageHeader pageHeader;
	PageMainZone pageMainZone;
	Betslip betslip;
	
	public BettingSteps(World world) {
		this.world = world;
		driver = (WebDriver) world.getResource("webdriver");
		
		pageHeader = (PageHeader) world.getResource("pageHeader");
		pageMainZone = (PageMainZone) world.getResource("pageMainZone");
		betslip = (Betslip) world.getResource("betslip");
		
		if (pageHeader==null) {
			pageHeader = new PageHeader(driver);
			world.addItem("pageHeader", pageHeader);
		}
		if (pageMainZone==null) {
			pageMainZone = new PageMainZone(driver);
			world.addItem("pageMainZone", pageMainZone);
		}
		if (betslip==null) {
			betslip = new Betslip(driver);
			world.addItem("betslip", betslip);
		}
		
	}
	
	@Given("^I have access to Betfair's website$")
	public void i_have_access_to_Betfair_s_website() throws Throwable {
		driver.navigate().to((String) world.getResource("pageLink"));
	}
	
	@Given("^I have navigated to Betfair's English Premier League football event Tottenham v Man City$")
	public void i_have_navigated_to_Betfair_s_English_Premier_League_football_event_Tottenham_v_Man_City() throws Throwable {
		pageHeader.clickSportsbookTab();
		pageHeader.clickSportsbookSubTab("Fotbal");
		pageMainZone.clickBrowseAll();
		pageMainZone.clickCountryByName("English Soccer");
		pageMainZone.clickCompetitionByName("English Premier League");
	}
	
	@Given("^I have selected the home selection as my betable selection of (\\d+)\\.(\\d+) odds$")
	public void i_have_selected_the_home_selection_as_my_betable_selection_of_odds(int arg1, int arg2) throws Throwable {
		pageMainZone.clickHomeSelectionForEvent("Tottenham v Man City");
	}

	@Given("^I have selected €(.*) as my bet stake$")
	public void i_have_selected_€_as_my_bet_stake(String stake) throws Throwable {
		betslip.addStake(stake);
		world.addItem("stake", stake);
	}
	
	@When("^I submit my betslip$")
	public void i_submit_my_betslip() throws Throwable {
	    betslip.clickPlaceBetslip();
	}
	
	
	@Then("^I can see my bet successfully placed$")
	public void i_can_see_my_bet_successfully_placed() throws Throwable {
		Assert.assertTrue(betslip.isBetPlaced());
	}
	
	@Then("^I can see the returns offered of €(.*)$")
	public void i_can_see_the_returns_offered_of(String potentialExpected) throws Throwable {
		potentialExpected = potentialExpected.replace('.', ',');
		Assert.assertTrue(betslip.getPotentialPayoutDisplayed().contains(potentialExpected));
	}

	@Given("^I have navigated to any Betfair English Premier League football event$")
	public void i_have_navigated_to_any_Betfair_English_Premier_League_football_event() throws Throwable {
		pageHeader.clickSportsbookTab();
		pageHeader.clickSportsbookSubTab("Fotbal");
		pageMainZone.clickBrowseAll();
		pageMainZone.clickCountryByName("English Soccer");
		pageMainZone.clickCompetitionByName("English Premier League");
		System.out.println("Picking the following random event:");
		WebElement event = pageMainZone.getRandomEvent();
		world.addItem("WeEvent", event);
		System.out.println(event.findElement(By.xpath(".//*[contains(@class,'details-event')]//a")).getAttribute("data-event"));
		System.out.println("------------------------");
	}
	
	@Given("^I have selected the (.*) selection as my betable selection$")
	public void i_have_selected_the_home_selection_as_my_betable_selection(String selectionType) throws Throwable {
		WebElement selection;
		if (selectionType.toLowerCase().contains("home")) {
			selection = pageMainZone.selectionFromEventByTypeElement((WebElement) world.getResource("WeEvent"), "0");
		}else if (selectionType.toLowerCase().contains("draw") || selectionType.toLowerCase().contains("tie")) {
			selection = pageMainZone.selectionFromEventByTypeElement((WebElement) world.getResource("WeEvent"), "1");
		} else {
			selection = pageMainZone.selectionFromEventByTypeElement((WebElement) world.getResource("WeEvent"), "2");
		}
		world.addItem("WeSelection", selection);
		System.out.println("Home selection price: " + selection.getText());
		world.addItem("selectionPrice", selection.getText());
		System.out.println("------------------------");
		selection.click();
	}
	
	@Then("^I can see the returns offered proportional to the selection's odds$")
	public void i_can_see_the_returns_offered_proportional_to_the_selection_s_odds() throws Throwable {	
		String potentialPayoutDisplayed = betslip.getPotentialPayoutDisplayed();
		System.out.println("Returns displayed: " + potentialPayoutDisplayed);
		float calculatedPp = Float.valueOf((String) world.getResource("stake")) * Float.valueOf((String) world.getResource("selectionPrice")); 
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_EVEN);
		System.out.println("Returns calculated: " +df.format(calculatedPp));
		System.out.println("------------------------");
		System.out.println("Asserting returns");
		Assert.assertTrue(potentialPayoutDisplayed.replace(',', '.').contains(df.format(calculatedPp).toString()));
	}

	@Then("^I can see the potential payout for my bet$")
	public void i_can_see_in_my_receipt_the_returns_offered_proportional_to_the_selection_s_odds() throws Throwable {
		String potentialPayoutDisplayed = betslip.getTicketReturns();
		System.out.println("Returns displayed: " + potentialPayoutDisplayed);
		float calculatedPp = Float.valueOf((String) world.getResource("stake")) * Float.valueOf((String) world.getResource("selectionPrice")); 
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_EVEN);
		System.out.println("Returns calculated: " +df.format(calculatedPp));
		System.out.println("------------------------");
		System.out.println("Asserting returns");
		Assert.assertTrue(potentialPayoutDisplayed.replace(',', '.').contains(df.format(calculatedPp).toString()));
	}
	
	
}
