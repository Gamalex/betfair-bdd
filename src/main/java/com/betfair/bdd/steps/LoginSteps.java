package com.betfair.bdd.steps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.betfair.TestRunner;
import com.betfair.bdd.data.World;
import com.betfair.bdd.pageobjects.Login;
import com.betfair.bdd.pageobjects.PageHeader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps extends TestRunner{


	private World world;
	private WebDriver driver;
	
	Login login;
	PageHeader pageHeader;
	
	public LoginSteps(World world) {
		this.world = world;
		driver = (WebDriver) world.getResource("webdriver");
		login = (Login) world.getResource("login");
		pageHeader = (PageHeader) world.getResource("pageHeader");
		
		if (login==null) {
			login = new Login(driver);
			world.addItem("login", login);
		}
		if (pageHeader==null) {
			pageHeader = new PageHeader(driver);
			world.addItem("pageHeader", pageHeader);
		}
	}

	@Then("^I am requested to login through the popup form$")
	public void i_am_requested_to_login() throws Throwable {
		System.out.println("------------------------");
		world.addItem("loginPage", "popup");
		System.out.println("Asserting popup displayed");		
		Assert.assertTrue(login.isLoginPopupDisplayed());
	}
	
	
	@When("^I submit my credentials$")
	public void i_submit_my_credentials() throws Throwable {
	    String loginPage = (String) world.getResource("loginPage");
	    if (loginPage!=null) {
	    	if(loginPage.contains("popup")){
				login.addUsername((String) world.getResource("user"));
				login.addPassword((String) world.getResource("password"));
				login.clickLogin();	    		
	    	}
		} else {
			pageHeader.addUsername((String) world.getResource("user"));
			pageHeader.addPassword((String) world.getResource("password"));
			pageHeader.clicklogin();
		}
	}
	
	@Then("^I am successfully logged in$")
	public void i_am_successfully_logged_in() throws Throwable {
		Assert.assertTrue(login.isUserLoggedIn());
	}
	
	
	@Given("^I am logged into my Betfair account$")
	public void i_am_logged_into_my_Betfair_account() throws Throwable {
		driver.navigate().to((String) world.getResource("pageLink"));
		i_submit_my_credentials();
		i_am_successfully_logged_in();
	}

	
	@When("^I access Betfair's website on my mobile$")
	public void i_access_Betfair_s_website_on_my_mobile() throws Throwable {
		driver.navigate().to((String) world.getResource("pageLink"));
	}
	
	@Then("^the mobile version of the website is loaded$")
	public void the_mobile_version_of_the_website_is_loaded() throws Throwable {
		Assert.assertTrue(driver.findElement(By.xpath("//body")).getAttribute("class").contains("mobile"));
	}
	

}
