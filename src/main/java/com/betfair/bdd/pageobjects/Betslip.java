package com.betfair.bdd.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.betfair.bdd.pageobjects.base.PageObject;

public class Betslip extends PageObject{

	public Betslip(WebDriver webDriver) {
		super(webDriver, "//div[1]");
	}


	/*
	 * Locators
	 */
	// betslip build
	private final String inputStakeLocator = "//div[contains(@class,'bet-information-inner-top')]//input[contains(@type,'text')]";
	private final String potentialLocator = "//span[contains(@class,'potential')]";
	private final String placeBetLocator = "//button[contains(@data-betslip-action,'place-bets')]";
	// betslip receipt
	private final String ticketReturnsLocator = "//span[contains(@class,'condensed-potential-winnings')]";
	private final String confirmedPlacementLocator = "//span[contains(@class,'confirmed-bets-message')]";
	
	/*
	 * Elements
	 */

	private WebElement inputStakeElement() {
		List<WebElement> element = getElementsFromLocator(inputStakeLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	private WebElement potentialElement() {
		List<WebElement> element = getElementsFromLocator(potentialLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement placebetElement() {
		List<WebElement> element = getElementsFromLocator(placeBetLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	private WebElement ticketReturnsElement() {
		List<WebElement> element = getElementsFromLocator(ticketReturnsLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	private WebElement confirmedPlacementElement() {
		WebElement element = getElementFromLocator(confirmedPlacementLocator, null);
		return element;
	}
	/*
	 * Methods
	 */
	
	public void addStake(String stake){
		sendKeys(inputStakeElement(), "stake", stake);
	}
	
	public String getPotentialPayoutDisplayed(){
		return getText(potentialElement(), "potential payout");
	}
	
	public void clickPlaceBetslip(){
		 clickButton(placebetElement(), "Plasează pariuri");
	}
	
	public String getTicketReturns(){
		return getText(ticketReturnsElement(), "Câştig potenţial");
	}
	
	public boolean isBetPlaced() throws InterruptedException{
		Thread.sleep(1500);
		return isElementDisplayed(confirmedPlacementElement());
	}
}
