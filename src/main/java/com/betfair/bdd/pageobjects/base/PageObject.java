package com.betfair.bdd.pageobjects.base;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject extends PageObjectBase{


	private final String pageLocator;
	private final Actions actions;


	public PageObject(WebDriver webDriver, String pageLocator) {
		super(webDriver);
		this.pageLocator = pageLocator;
		this.actions = new Actions(webDriver);
	}

	@Override
	public Boolean isDisplayed() {
		return page().isDisplayed();
	}


	/*
	 * Common Methods - General
	 */

	/**
	 * Returns <b>true</b> if the given element is displayed, <b>false</b> otherwise.
	 * @param element
	 * @return
	 */
	protected Boolean isElementDisplayed(WebElement element) {
		return element.isDisplayed();
	}

	/**
	 * Returns <b>true</b> if the given element is enabled, <b>false</b> otherwise.
	 * @param element
	 * @return
	 */
	protected Boolean isElementEnabled(WebElement element) {
		return element.isEnabled();
	}
	

	/*
	 * Common Methods - Links
	 */

	/**
	 * Clicks the given link.
	 * @param link
	 */
	protected void clickLink(WebElement link) {
		System.out.println("Clicking the [" + link.getText().trim() + "] link...");
		link.click();
	}

	
	/**
	 * Returns the <b>value</b> attribute (if available) or <b>text</b> (if value is missing) of the given element.
	 * @param element
	 * @return
	 */
	protected String getElementText(WebElement element) {
		if (element.getAttribute("value") != null) {
			return element.getAttribute("value");
		}
		return element.getText().trim();
	}
	
	protected List<WebElement> getElementsFromLocator(String locator, String...args) {
		// force the Thread to wait 30 seconds in order for the element to be visible
		// not an elegant implementation; needs to be refined
		List<WebElement> elements = null;
		for (int i = 0; i < 300; i++) {
			try {
				elements = getWebDriver().findElements(By.xpath(String.format(locator, (Object[]) args)));
				if (!elements.isEmpty()) {
					break;
				} else if (elements.get(0).isDisplayed()) {
					break;
				}
			} catch (Exception e) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		return elements;
	}

	protected WebElement getElementFromLocator(String locator, String...args) {
		List<WebElement> element = getWebDriver().findElements(By.xpath(String.format(locator, (Object[]) args)));
		if (element.isEmpty()) {
			return null;
		}
		else {
			actions.moveToElement(element.get(0));
			return element.get(0);
		}
	}
	
	protected WebElement page() {
		return getElementFromLocator(pageLocator);
	}
	
	

	/**
	 * Clicks the given button.
	 * @param button
	 * @param buttonName
	 */
	protected void clickButton(WebElement button, String buttonName) {
		System.out.println("Clicking the [" + buttonName + "] button...");
		button.click();
	}

	/*
	 * Common Methods - Radio Button
	 */

	/**
	 * Clicks the given button.
	 * @param button
	 * @param buttonName
	 */
	protected void clickRadioButton(WebElement radioButton, String radioButtonName) {
		System.out.println("Clicking the [" + radioButtonName + "] radio button...");
		radioButton.click();
	}

	/*
	 * Common Methods - Checkboxes
	 */

	/**
	 * Clicks the given checkbox.
	 * @param checkbox
	 * @param checkboxName	print-friendly checkbox name, used for logging
	 */
	protected void clickCheckbox(WebElement checkbox, String checkboxName) {
		System.out.println("Clicking the [" + checkboxName + "] checkbox...");
		checkbox.click();		
	}

	/**
	 * Returns <b>true</b> if the given checkbox is ticked, <b>false</b> otherwise.
	 * @param checkbox
	 * @return
	 */
	protected Boolean getCheckboxState(WebElement checkbox) {
		return checkbox.isSelected();
	}

	/*
	 * Common Methods - Textboxes 
	 */
	/**
	 * Sends keys to the textbox element
	 * @param textbox WebElement
	 * @param textboxName descriptive name for the textbox
	 * @param text keys sent
	 */
	protected void sendKeys(WebElement textbox, String textboxName, String text){
		System.out.println("Typing [" + text + "] in the [" + textboxName + "] textbox...");
		textbox.sendKeys(text);
	}
	
	protected void sendKeysSilent(WebElement textbox, String textboxName, String text){
		System.out.println("Typing in the [" + textboxName + "] textbox...");
		textbox.sendKeys(text);
	}
	/**
	 * Grabbing text from the WebElement field
	 * @param field
	 * @param fieldName
	 * @return text displayed
	 */
	protected String getText(WebElement field, String fieldName){
		System.out.println("Grabbing text from the [" + fieldName + "] field...");
		return field.getText();
	}
}
