package com.betfair.bdd.pageobjects.base;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public abstract class PageObjectBase {

	private WebDriver webDriver;
	
	public PageObjectBase(WebDriver webDriver) {
		this.webDriver = webDriver;
		PageFactory.initElements(webDriver, this);
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}
	
	public String getPageTitle() {
		return this.webDriver.getTitle();
	}

	/**
	 * Returns the visibility of the page object, dictated by the visibility
	 * of all the static web elements on the page object.
	 * 
	 * @return true if the page object is displayed, and false otherwise.
	 */
	public abstract Boolean isDisplayed();
	
	
	/**
	 * Retrieve Web Element from web page
	 *
	 * @param by By type (cssSelector, id or xpath)
	 * @return WebElement if found, null otherwise
     */
	public WebElement getElementBy(By by){
		try {
			return this.getWebDriver().findElement(by);
		} catch (NoSuchElementException ignored){}

		return null;
	}
	
	/**
	 * Retrieve Web Element from web page using id
	 *
	 * @param id String containing which Id name to find
	 * @return WebElement if found, MissingWebElement otherwise
	 */
	public WebElement getElementById(String id){
		return getElementBy(By.id(id));
	}
	
	
}
