package com.betfair.bdd.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.betfair.bdd.pageobjects.base.PageObject;

public class Login extends PageObject{

	public Login(WebDriver webDriver) {
		super(webDriver, "//div[1]");
	}


	/*
	 * Locators
	 */
	
	// pop up elements
	private final String popWindowLocator = "//div[contains(@class,'yui3-widget yui3-dialog login-lightBox-container yui3-dialog-focused')]";
	private final String popUsernameLocator = "//*[@id='ll-username']";
	private final String popPasswordLocator = "//*[@id='ll-password']";
	private final String popLoginLocator = "//*[@id='lightBox-input-submit']";
	
	// header elements
	private final String headerAccountLocator = "//div[contains(@class,'ssc-ud ssc-udrl')]";
	
	/*
	 * Elements
	 */

	private WebElement popWindowElement() {
		List<WebElement> element = getElementsFromLocator(popWindowLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement popUsernameElement() {
		List<WebElement> element = getElementsFromLocator(popUsernameLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	private WebElement popPasswordElement() {
		List<WebElement> element = getElementsFromLocator(popPasswordLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement popLoginElement() {
		List<WebElement> element = getElementsFromLocator(popLoginLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	private WebElement headerAccountElement() {
		List<WebElement> element = getElementsFromLocator(headerAccountLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	/*
	 * Methods
	 */

	public boolean isLoginPopupDisplayed(){
		return isElementDisplayed(popWindowElement());
	}
	
	public void addUsername(String username){
		sendKeys(popUsernameElement(), "Nume de utilizator", username);
	}

	public void addPassword(String password){
		sendKeys(popPasswordElement(), "Parolă", password);
	}
	
	public void clickLogin(){
		clickButton(popLoginElement(), "Conectare");
	}
	
	public boolean isUserLoggedIn(){
		return isElementDisplayed(headerAccountElement());
	}

}
