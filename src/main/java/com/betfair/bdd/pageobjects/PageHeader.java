package com.betfair.bdd.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.betfair.bdd.pageobjects.base.PageObject;

public class PageHeader extends PageObject{

	public PageHeader(WebDriver webDriver) {
		super(webDriver, "//*[@id='ssc-hmw']");
	}


	/*
	 * Locators
	 */

	// tabs
	private final String sportsbookLocator = "//*[@id='SPORTSBOOK']";
	private final String sportsbookListLocator = "//*[contains(@class,'main-sports-list')]//span[contains(.,'%s')]";
	
	// login and account
	private final String loginUserLocator = "//input[contains(@id,'ssc-liu')]";
	private final String loginPasswordLocator = "//input[contains(@id,'ssc-lipw')]";
	private final String loginButtonLocator = "//input[contains(@class,'ssc-cta ssc-cta-primary')]";
	
	/*
	 * Elements
	 */

	private WebElement sportsbookElement() {
		List<WebElement> element = getElementsFromLocator(sportsbookLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement sportsbookListElement(String sportsbookSubtabName){
		List<WebElement> element = getElementsFromLocator(sportsbookListLocator, sportsbookSubtabName);
		return element.isEmpty() ? null : element.get(0);
	}
	

	private WebElement loginUserElement(){
		List<WebElement> element = getElementsFromLocator(loginUserLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement loginPasswordElement(){
		List<WebElement> element = getElementsFromLocator(loginPasswordLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}

	private WebElement loginButtonElement(){
		List<WebElement> element = getElementsFromLocator(loginButtonLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	/*
	 * Methods
	 */
	
	public void clickSportsbookTab(){
		clickButton(sportsbookElement(), "Sportsbook");
	}
	
	public void clickSportsbookSubTab(String sportsbookSubtabName){
		clickButton(sportsbookListElement(sportsbookSubtabName), sportsbookSubtabName);
	}

	public void addUsername(String username){
		sendKeysSilent(loginUserElement(), "Nume de utilizator", username);
	}

	public void addPassword(String password){
		sendKeysSilent(loginPasswordElement(), "Parolă", password);
	}
	
	public void clicklogin(){
		clickButton(loginButtonElement(), "Conectare");
	}
	
}
