package com.betfair.bdd.pageobjects;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.betfair.bdd.pageobjects.base.PageObject;

public class PageMainZone extends PageObject{

	public PageMainZone(WebDriver webDriver) {
		super(webDriver, ".//*[@id='zone-main']");
	}
	
	
	/*
	 * Locators
	 */
	/**
	 * competitions and coupons quick filters
	 */
	//private final String browseAllLocator = "//a[contains(@class,'ui-nav browse-all ui-clickselect')]";
	//private final String browseAllLocator = "//span[contains(@class,'browse-all-text')]";
	private final String browseAllLocator = "//a[contains(@class,'ui-nav browse-all-arrow')]";
	
	/**
	 * browse all competitions
	 */
	// class hierarchy
	private final String countryLocator = "//ul[contains(@class,'browseall-country-selector ui-clickselect-container')]//a[contains(.,'%s')]";
	// type hierarchy
	private final String competitionLocator = "//ul[contains(@class,'browseall-events-selector')]//a[contains(.,'%s')]";
	
	/**
	 * events/markets list
	 */
	private final String eventMarketSelectionLocator = "//div[contains(@class,'details-event')]//*[contains(@data-event,'%s')]/../../..//li[contains(@class,'selection sel-%s')]/a";
	private final String dateListLocator = "//ul[contains(@class,'section-list')]/*";
	private final String eventListocator = "//ul[contains(@class,'section-list')]/*[%s]/ul/*/*";
	private final String eventByDateAndPositionLocator = "//ul[contains(@class,'section-list')]/*[%s]/ul/*[%s]/*";
	private final String selectionByTypeLocator = ".//*[contains(@class,'selection sel-%s')]";
	
	/*
	 * Elements
	 */

	private WebElement browseAllFilterElement() {
		List<WebElement> element = getElementsFromLocator(browseAllLocator, null);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement countryElement(String name) {
		WebElement element = getElementFromLocator(countryLocator, name);
		return element;
	}
	
	private WebElement competitionElement(String name) {
		List<WebElement> element = getElementsFromLocator(competitionLocator, name);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private WebElement eventMarketSelectionElement(String eventName, String selectionNumber) {
		List<WebElement> element = getElementsFromLocator(eventMarketSelectionLocator, eventName, selectionNumber);
		return element.isEmpty() ? null : element.get(0);
	}
	
	private List<WebElement> dateListElement() {
		List<WebElement> element = getElementsFromLocator(dateListLocator, null);
		return element.isEmpty() ? null : element;
	}

	private List<WebElement> eventListElement(String date) {
		List<WebElement> element = getElementsFromLocator(eventListocator, date);
		return element.isEmpty() ? null : element;
	}

	private WebElement eventByDateAndPositionElement(String date, String position) {
		List<WebElement> element = getElementsFromLocator(eventByDateAndPositionLocator, date, position);
		return element.isEmpty() ? null : element.get(0);
	}
	
	/**
	 * 
	 * @param type can only contain 0, 1 or 2 (as Home, Draw, Away selections)
	 * @return
	 */
	public WebElement selectionFromEventByTypeElement(WebElement event, String type) {
		List<WebElement> element = getElementsFromLocator(selectionByTypeLocator, type);
		return element.isEmpty() ? null : element.get(0);
	}
	/*
	 * Methods
	 */
	
	public void clickBrowseAll() throws InterruptedException{
		for (int i = 0; i < 10; i++) {
			try {
				clickButton(browseAllFilterElement(), "Răsfoire toate");
				break;
			} catch (Exception e) {
				Thread.sleep(1000);
			}
		}
		
	}
	
	public void clickCountryByName(String name) throws InterruptedException{
		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor)getWebDriver();
		jse.executeScript("scroll(0, 250)"); // if the element is on bottom.
		clickButton(countryElement(name), name);
	}
	
	public void clickCompetitionByName(String name){
		clickButton(competitionElement(name), name);
	}
	
	public void clickHomeSelectionForEvent(String eventName){
		clickButton(eventMarketSelectionElement(eventName, "0"),eventName+"'s Home selection");
		System.out.println();
	}

	
	/*
	 * Counts and random
	 */
	
	private int count(List<WebElement> webElementList){
		return webElementList.size();
	}
	
	private int getRandomDate(){
		int max = count(dateListElement());
		Random rand = new Random();
		return rand.nextInt(max) + 1;		
	}
	
	private int getRandomeEvent(int date){
		int max = count(eventListElement(String.valueOf(date)));
		Random rand = new Random();
		return rand.nextInt(max) + 1;
	}
	
	public WebElement getRandomEvent(){
		int date = getRandomDate();
		int position = getRandomeEvent(date);
		return eventByDateAndPositionElement(String.valueOf(date), String.valueOf(position));
	}
}
