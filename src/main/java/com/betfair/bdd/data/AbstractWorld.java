package com.betfair.bdd.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public abstract class AbstractWorld {

	private HashMap<String, Object> items;
	
	protected AbstractWorld() {
		items = new HashMap<String, Object>();
		
		/*
		 * loading properties from config.property
		 */
		Properties properties = new Properties();
		InputStream input = null;
		try {

			input = new FileInputStream("src/main/resources/properties/config.properties");
			// load a properties file
			properties.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		/*
		 * adding configs in world
		 */
		for (Object item : properties.keySet()) {
			addItem((String) item, properties.get(item));
		}
	}
	
	/**
	 * Add any Object item in the list
	 * @param string identifying name for the Object item
	 * @param item Object item stored in the list
	 */
	public void addItem(String string, Object item) {
		items.put(string, item);		
	}

	
	/**
	 * Returns a particular object that has been added. Needs to be cast when
	 * called as the return type is Object.
	 */
	public Object getResource(String nameOfResource) {
		return items.get(nameOfResource);
	}

}
