package com.betfair.bdd.data;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
public class World extends AbstractWorld{

	
	public World() throws IOException {
		super();
	}

	/*
	 * launch specific driver before each scenario
	 * based on the unique @DEVICE_ tag per scenario
	 * 
	 * if scenario has DEVICE_any => launch default.device from config.properties webdriver
	 * if default.device is not mobile => launch desktop webdriver
	 */
	@Before
	public void before(Scenario scenario) {	  
		// handle device tags
		String device = "";
	    for (String tag : scenario.getSourceTagNames()) {
		    if (tag.contains("DEVICE_any")) {
				device = (String) getResource("default.device");
			} else if (tag.contains("DEVICE_mobile")) {
				device = "mobile";
			} else if (tag.contains("DEVICE_desktop")) {
				device = "desktop";
			} else {
				device = "desktop";
			}
		}
	    
	    // launch driver based on the device identified
	    if (device.contains("mobile")) {
	    	WebDriver driver;
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--user-agent=Mobile");
			System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
			driver = new ChromeDriver(options);
			addItem("webdriver", driver);
		} else {
			// desktop - as default
	    	WebDriver driver;
			System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver(options);
			addItem("webdriver", driver);
		} 
	}
	
	// closing webdriver after each scenario
	@After
	public void closeDriver(){
		WebDriver driver = (WebDriver) getResource("webdriver");
		driver.quit();
	}
}
