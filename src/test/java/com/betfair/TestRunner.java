package com.betfair;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.junit.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/main/resources" , monochrome = true,
	plugin = { "pretty", "html:target/cucumber", "json:target/json/cucumber.json","junit:target/junit/cucumber.xml" },
	glue = {"com.betfair.bdd.steps", "com.betfair.bdd.data"},
	name = "")
public class TestRunner {

	@BeforeClass
	public static void beforeTestRun() {
	}
	
	@AfterClass
	public static void afterTestRun() {
	}
}
