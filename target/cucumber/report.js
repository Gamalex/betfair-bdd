$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/BetslipPlacementAndReturns.feature");
formatter.feature({
  "line": 1,
  "name": "Bet placement and returns on English Premier League",
  "description": "As a Betfair Customer \r\nI want the ability to place a bet on a English Premier League event\r\nSo that I can win",
  "id": "bet-placement-and-returns-on-english-premier-league",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 7,
  "name": "Potential payout is correctly calculated on any Premier League market for non-logged in users",
  "description": "",
  "id": "bet-placement-and-returns-on-english-premier-league;potential-payout-is-correctly-calculated-on-any-premier-league-market-for-non-logged-in-users",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 6,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "I have access to Betfair\u0027s website",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I have navigated to any Betfair English Premier League football event",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I have selected the \u003cselection\u003e selection as my betable selection",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I have selected €\u003cstake\u003e as my bet stake",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I can see the returns offered proportional to the selection\u0027s odds",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "bet-placement-and-returns-on-english-premier-league;potential-payout-is-correctly-calculated-on-any-premier-league-market-for-non-logged-in-users;",
  "rows": [
    {
      "cells": [
        "selection",
        "stake"
      ],
      "line": 15,
      "id": "bet-placement-and-returns-on-english-premier-league;potential-payout-is-correctly-calculated-on-any-premier-league-market-for-non-logged-in-users;;1"
    },
    {
      "cells": [
        "home",
        "0.05"
      ],
      "line": 16,
      "id": "bet-placement-and-returns-on-english-premier-league;potential-payout-is-correctly-calculated-on-any-premier-league-market-for-non-logged-in-users;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 1840079675,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Potential payout is correctly calculated on any Premier League market for non-logged in users",
  "description": "",
  "id": "bet-placement-and-returns-on-english-premier-league;potential-payout-is-correctly-calculated-on-any-premier-league-market-for-non-logged-in-users;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 6,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "I have access to Betfair\u0027s website",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I have navigated to any Betfair English Premier League football event",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I have selected the home selection as my betable selection",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I have selected €0.05 as my bet stake",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I can see the returns offered proportional to the selection\u0027s odds",
  "keyword": "Then "
});
formatter.match({
  "location": "BettingSteps.i_have_access_to_Betfair_s_website()"
});
formatter.result({
  "duration": 5423843807,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_have_navigated_to_any_Betfair_English_Premier_League_football_event()"
});
formatter.result({
  "duration": 5593949351,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 20
    }
  ],
  "location": "BettingSteps.i_have_selected_the_home_selection_as_my_betable_selection(String)"
});
formatter.result({
  "duration": 80636831,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.05",
      "offset": 17
    }
  ],
  "location": "BettingSteps.i_have_selected_€_as_my_bet_stake(String)"
});
formatter.result({
  "duration": 310783701,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_can_see_the_returns_offered_proportional_to_the_selection_s_odds()"
});
formatter.result({
  "duration": 54600119,
  "status": "passed"
});
formatter.after({
  "duration": 1353192787,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 19,
  "name": "Placing a bet and calculating returns on any Premier League market for logged in users",
  "description": "",
  "id": "bet-placement-and-returns-on-english-premier-league;placing-a-bet-and-calculating-returns-on-any-premier-league-market-for-logged-in-users",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 18,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 20,
  "name": "I am logged into my Betfair account",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "I have navigated to any Betfair English Premier League football event",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I have selected the \u003cselection\u003e selection as my betable selection",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I have selected €\u003cstake\u003e as my bet stake",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "I submit my betslip",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I can see my bet successfully placed",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I can see the potential payout for my bet",
  "keyword": "And "
});
formatter.examples({
  "line": 28,
  "name": "",
  "description": "",
  "id": "bet-placement-and-returns-on-english-premier-league;placing-a-bet-and-calculating-returns-on-any-premier-league-market-for-logged-in-users;",
  "rows": [
    {
      "cells": [
        "selection",
        "stake"
      ],
      "line": 29,
      "id": "bet-placement-and-returns-on-english-premier-league;placing-a-bet-and-calculating-returns-on-any-premier-league-market-for-logged-in-users;;1"
    },
    {
      "cells": [
        "home",
        "0.20"
      ],
      "line": 30,
      "id": "bet-placement-and-returns-on-english-premier-league;placing-a-bet-and-calculating-returns-on-any-premier-league-market-for-logged-in-users;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 1306411746,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Placing a bet and calculating returns on any Premier League market for logged in users",
  "description": "",
  "id": "bet-placement-and-returns-on-english-premier-league;placing-a-bet-and-calculating-returns-on-any-premier-league-market-for-logged-in-users;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 18,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 20,
  "name": "I am logged into my Betfair account",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "I have navigated to any Betfair English Premier League football event",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I have selected the home selection as my betable selection",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I have selected €0.20 as my bet stake",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "I submit my betslip",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I can see my bet successfully placed",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I can see the potential payout for my bet",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.i_am_logged_into_my_Betfair_account()"
});
formatter.result({
  "duration": 7852978167,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_have_navigated_to_any_Betfair_English_Premier_League_football_event()"
});
formatter.result({
  "duration": 4623776654,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 20
    }
  ],
  "location": "BettingSteps.i_have_selected_the_home_selection_as_my_betable_selection(String)"
});
formatter.result({
  "duration": 75339681,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.20",
      "offset": 17
    }
  ],
  "location": "BettingSteps.i_have_selected_€_as_my_bet_stake(String)"
});
formatter.result({
  "duration": 309623540,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_submit_my_betslip()"
});
formatter.result({
  "duration": 65058791,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_can_see_my_bet_successfully_placed()"
});
formatter.result({
  "duration": 1528418065,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_can_see_in_my_receipt_the_returns_offered_proportional_to_the_selection_s_odds()"
});
formatter.result({
  "duration": 32281418,
  "status": "passed"
});
formatter.after({
  "duration": 1294944670,
  "status": "passed"
});
formatter.uri("features/Login.feature");
formatter.feature({
  "line": 1,
  "name": "Users can place bets when logged in",
  "description": "As a logged in Betfair Customer \nI want the ability to place a bet on a English Premier League event\nSo that I can win",
  "id": "users-can-place-bets-when-logged-in",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1323903960,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "User can login from the page header",
  "description": "",
  "id": "users-can-place-bets-when-logged-in;user-can-login-from-the-page-header",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "I have access to Betfair\u0027s website",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I submit my credentials",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I am successfully logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "BettingSteps.i_have_access_to_Betfair_s_website()"
});
formatter.result({
  "duration": 5024129504,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_submit_my_credentials()"
});
formatter.result({
  "duration": 268330993,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_am_successfully_logged_in()"
});
formatter.result({
  "duration": 2923447374,
  "status": "passed"
});
formatter.after({
  "duration": 1291614111,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 13,
  "name": "Users can Login after setting up betslip",
  "description": "",
  "id": "users-can-place-bets-when-logged-in;users-can-login-after-setting-up-betslip",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 12,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "I have access to Betfair\u0027s website",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I have navigated to any Betfair English Premier League football event",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I have selected the \u003cselection\u003e selection as my betable selection",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I have selected €\u003cstake\u003e as my bet stake",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I submit my betslip",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I am requested to login through the popup form",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I submit my credentials",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "I am successfully logged in",
  "keyword": "Then "
});
formatter.examples({
  "line": 23,
  "name": "",
  "description": "",
  "id": "users-can-place-bets-when-logged-in;users-can-login-after-setting-up-betslip;",
  "rows": [
    {
      "cells": [
        "selection",
        "stake"
      ],
      "line": 24,
      "id": "users-can-place-bets-when-logged-in;users-can-login-after-setting-up-betslip;;1"
    },
    {
      "cells": [
        "home",
        "0.05"
      ],
      "line": 25,
      "id": "users-can-place-bets-when-logged-in;users-can-login-after-setting-up-betslip;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 1356434638,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Users can Login after setting up betslip",
  "description": "",
  "id": "users-can-place-bets-when-logged-in;users-can-login-after-setting-up-betslip;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 12,
      "name": "@DEVICE_desktop"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "I have access to Betfair\u0027s website",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I have navigated to any Betfair English Premier League football event",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I have selected the home selection as my betable selection",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I have selected €0.05 as my bet stake",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I submit my betslip",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I am requested to login through the popup form",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I submit my credentials",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "I am successfully logged in",
  "keyword": "Then "
});
formatter.match({
  "location": "BettingSteps.i_have_access_to_Betfair_s_website()"
});
formatter.result({
  "duration": 5004365355,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_have_navigated_to_any_Betfair_English_Premier_League_football_event()"
});
formatter.result({
  "duration": 5355892130,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 20
    }
  ],
  "location": "BettingSteps.i_have_selected_the_home_selection_as_my_betable_selection(String)"
});
formatter.result({
  "duration": 71718074,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.05",
      "offset": 17
    }
  ],
  "location": "BettingSteps.i_have_selected_€_as_my_bet_stake(String)"
});
formatter.result({
  "duration": 315737384,
  "status": "passed"
});
formatter.match({
  "location": "BettingSteps.i_submit_my_betslip()"
});
formatter.result({
  "duration": 71194994,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_am_requested_to_login()"
});
formatter.result({
  "duration": 147947763,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_submit_my_credentials()"
});
formatter.result({
  "duration": 231545294,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.i_am_successfully_logged_in()"
});
formatter.result({
  "duration": 3798479585,
  "status": "passed"
});
formatter.after({
  "duration": 1363447652,
  "status": "passed"
});
formatter.uri("features/MobilePageAccess.feature");
formatter.feature({
  "line": 1,
  "name": "User can access Betfair website on mobile",
  "description": "As a Betfair Customer \r\nI want the ability to access Betfair\u0027s website on my mobile\r\nSo that I can place bets",
  "id": "user-can-access-betfair-website-on-mobile",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1315809229,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Mobile devices can access Betfair Website",
  "description": "",
  "id": "user-can-access-betfair-website-on-mobile;mobile-devices-can-access-betfair-website",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@DEVICE_mobile"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "I access Betfair\u0027s website on my mobile",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "the mobile version of the website is loaded",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.i_access_Betfair_s_website_on_my_mobile()"
});
formatter.result({
  "duration": 4362608965,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.the_mobile_version_of_the_website_is_loaded()"
});
formatter.result({
  "duration": 25546989,
  "status": "passed"
});
formatter.after({
  "duration": 1229903841,
  "status": "passed"
});
});