Feature: Users can place bets when logged in
  As a logged in Betfair Customer 
  I want the ability to place a bet on a English Premier League event
  So that I can win
  
@DEVICE_desktop
Scenario: User can login from the page header
		Given I have access to Betfair's website
		When I submit my credentials
		Then I am successfully logged in
		
@DEVICE_desktop
Scenario Outline: Users can Login after setting up betslip
    Given I have access to Betfair's website
    And I have navigated to any Betfair English Premier League football event
    And I have selected the <selection> selection as my betable selection
    And I have selected €<stake> as my bet stake
    And I submit my betslip
    And I am requested to login through the popup form
    When I submit my credentials
    Then I am successfully logged in

    Examples: 
     | selection | stake |
     | home      |  0.05 |
