Feature: User can access Betfair website on mobile
  As a Betfair Customer 
  I want the ability to access Betfair's website on my mobile
  So that I can place bets
  
@DEVICE_mobile
Scenario: Mobile devices can access Betfair Website
	When I access Betfair's website on my mobile
	Then the mobile version of the website is loaded
	